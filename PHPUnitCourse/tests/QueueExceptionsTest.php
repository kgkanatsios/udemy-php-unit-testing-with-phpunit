<?php

use PHPUnit\Framework\TestCase;

class QueueExceptionsTest extends TestCase
{
    protected $queue;

    protected function setUp(): void
    {
        // this function run before each test
        $this->queue = new Queue;
    }

    protected function tearDown(): void
    {
        // this function run before each test
        unset($this->queue);
    }

    public function testNewQueueIsEmpty()
    {
        $this->assertEquals(0, $this->queue->getCount());
    }

    public function testAnItemIsAddedToTheQueue()
    {
        $this->queue->push('Kostas');
        $this->assertEquals(1, $this->queue->getCount());
    }

    public function testAnItemIsRemovedFromTheQueue()
    {
        $this->queue->push('Kostas');

        $this->assertEquals('Kostas', $this->queue->pop());
        $this->assertEquals(0, $this->queue->getCount());
    }

    public function testAnItemIsRemovedFromTheFrontOfTheQueue()
    {
        $this->queue->push('Kostas');
        $this->queue->push('Gkanatsios');

        $this->assertEquals('Kostas', $this->queue->pop());
        $this->assertEquals('Gkanatsios', $this->queue->pop());
    }

    public function testMaxNumberOfItemsCanBeAdded()
    {
        for ($i = 0; $i < Queue::MAX_ITEMS; $i++) {
            $this->queue->push($i);
        }
        $this->assertEquals(Queue::MAX_ITEMS, $this->queue->getCount());

        return $this->queue;
    }
    /**
     * @depends testMaxNumberOfItemsCanBeAdded
     */
    public function testExceptionThrownWhenAddingAnItemToAFullQueue(Queue $queue)
    {
        $this->expectException(QueueException::class);
        $this->expectExceptionMessage("Queue is full");
        $queue->push(5);
    }
}

<?php

use PHPUnit\Framework\TestCase;

class QueueDependencyTest extends TestCase
{
    public function testNewQueueIsEmpty()
    {
        $queue = new Queue;
        $this->assertEquals(0, $queue->getCount());

        return $queue;
    }
    /**
     * @depends testNewQueueIsEmpty
     */
    public function testAnItemIsAddedToTheQueue(Queue $queue)
    {
        $queue->push('Kostas');

        $this->assertEquals(1, $queue->getCount());

        return $queue;
    }

    /**
     * @depends testAnItemIsAddedToTheQueue
     */

    public function testAnItemIsRemovedFromTheQueue(Queue $queue)
    {
        $item = $queue->pop();

        $this->assertEquals('Kostas', $item);
        $this->assertEquals(0, $queue->getCount());
    }
}

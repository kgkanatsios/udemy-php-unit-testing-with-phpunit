<?php

use PHPUnit\Framework\TestCase;

class QueueSetUpTest extends TestCase
{
    protected $queue;

    protected function setUp(): void
    {
        // this function run before each test
        $this->queue = new Queue;
    }

    protected function tearDown(): void
    {
        // this function run before each test
        unset($this->queue);
    }

    public static function setUpBeforeClass(): void
    {
        // this function run before all tests
    }

    public static function tearDownAfterClass(): void
    {
        // this function run after all tests
    }

    public function testNewQueueIsEmpty()
    {
        $this->assertEquals(0, $this->queue->getCount());
    }

    public function testAnItemIsAddedToTheQueue()
    {
        $this->queue->push('Kostas');
        $this->assertEquals(1, $this->queue->getCount());
    }

    public function testAnItemIsRemovedFromTheQueue()
    {
        $this->queue->push('Kostas');

        $this->assertEquals('Kostas', $this->queue->pop());
        $this->assertEquals(0, $this->queue->getCount());
    }

    public function testAnItemIsRemovedFromTheFrontOfTheQueue()
    {
        $this->queue->push('Kostas');
        $this->queue->push('Gkanatsios');

        $this->assertEquals('Kostas', $this->queue->pop());
        $this->assertEquals('Gkanatsios', $this->queue->pop());
    }
}

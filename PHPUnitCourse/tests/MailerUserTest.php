<?php

use Mockery\Adapter\Phpunit\MockeryTestCase;

class MailerUserTest extends MockeryTestCase
{
    public function testNotifyReturnsTrue()
    {
        $mailer_user = new MailerUser('info@gkanatsios.gr');

        $mailer_user->setMailer(new Mailer);
        $this->assertTrue($mailer_user->notifyDependencyInjection('Test Message'));
    }

    public function testNotifyReturnsTrueWithCallable()
    {
        $mailer_user = new MailerUser('info@gkanatsios.gr');

        $mailer_user->setMailerCallable(function () {
            echo 'Mocked';
            return true;
        });

        $this->assertTrue($mailer_user->notifyCallable('Test Message'));
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */

    public function testNotifyReturnsTrueWithMockery()
    {
        $mailer_user = new MailerUser('info@gkanatsios.gr');

        $mock = Mockery::mock('alias:Mailer');
        $mock->shouldReceive('send')->once()->with($mailer_user->email, 'Test Message')->andReturn(true);

        $this->assertTrue($mailer_user->notify('Test Message'));
    }
}

<?php
//./vendor/bin/phpunit ./tests/UserTest.php
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testReturnsFullName()
    {
        $user = new User;
        $user->first_name = "Kostas";
        $user->last_name = "Gkanatsios";

        $this->assertEquals("Kostas Gkanatsios", $user->getFullName());
    }
    public function testFullNameIsEmptyByDefault()
    {
        $user = new User;

        $this->assertEquals("", $user->getFullName());
    }

    /**
     * @test
     */

    public function userHasFirstName()
    {
        $user = new User;
        $user->first_name = "Kostas";

        $this->assertEquals("Kostas", $user->getFullName());
    }
    public function testNotificationSend()
    {
        $user = new User;

        $mock_mailer = $this->createMock(Mailer::class);
        $mock_mailer->expects($this->once())
            ->method('sendMessage')
            ->with($this->equalTo('info@gkanatsios.gr'), $this->equalTo('Hello Kostas'))
            ->willReturn(true);

        $user->setMailer($mock_mailer);

        $user->email = "info@gkanatsios.gr";
        $this->assertTrue($user->notify("Hello Kostas"));
    }
    public function testCannotNotifyUserWithNoEmail()
    {
        $user =  new User;
        $mock_mailer = $this->createMock(Mailer::class);
        $user->setMailer($mock_mailer);

        $mock_mailer->method('sendMessage')->will($this->throwException(new Exception('Email address is null!')));

        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Email address is null!");

        $user->notify("Hello Kostas");
    }
    public function testCannotNotifyUserWithNoEmailGetMockBuilder()
    {
        $user =  new User;
        $mock_mailer = $this->getMockBuilder(Mailer::class)->setMethods(null)->getMock();
        $user->setMailer($mock_mailer);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Email address is null!");

        $user->notify("Hello Kostas");
    }
}

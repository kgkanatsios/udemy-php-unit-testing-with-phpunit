<?php

use PHPUnit\Framework\TestCase;

class AbstractPersonTest extends TestCase
{
    public function testNameAndTitleIsReturned()
    {
        $developer = new Developer('Gkanatsios');
        $this->assertEquals('Dev Gkanatsios', $developer->getNameAndTitle());
    }

    public function testNameAndTitleIncludesValueFromGetTitle()
    {
        $mock = $this->getMockBuilder(AbstractPerson::class)->setConstructorArgs(['Gkanatsios'])->getMockForAbstractClass();
        $mock->method('getTitle')->willReturn('Dev');

        $this->assertEquals('Dev Gkanatsios', $mock->getNameAndTitle());
    }
}

<?php

use PHPUnit\Framework\TestCase;

class MailerTest extends TestCase
{
    public function testSendMessageReturnsTrue()
    {
        $this->assertTrue(Mailer::send('info@gkanatsios.gr', 'Test Message'));
    }

    public function testInvalidArgumentsExceptionIfEmailIsEmpty()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Email address is null!");
        Mailer::send('', 'Test Message');
    }
}

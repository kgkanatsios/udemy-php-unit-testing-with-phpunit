<?php

use PHPUnit\Framework\TestCase;

class QueueSetUpBeforeClassTest extends TestCase
{
    protected static $queue;

    protected function setUp(): void
    {
        // this function run before each test
        static::$queue->clear();
    }

    protected function tearDown(): void
    {
        // this function run before each test
    }

    public static function setUpBeforeClass(): void
    {
        // this function run before all tests
        static::$queue = new Queue;
    }

    public static function tearDownAfterClass(): void
    {
        // this function run after all tests
        static::$queue = null;
    }

    public function testNewQueueIsEmpty()
    {
        $this->assertEquals(0, static::$queue->getCount());
    }

    public function testAnItemIsAddedToTheQueue()
    {
        static::$queue->push('Kostas');
        $this->assertEquals(1, static::$queue->getCount());
    }

    public function testAnItemIsRemovedFromTheQueue()
    {
        static::$queue->push('Kostas');
        $this->assertEquals('Kostas', static::$queue->pop());
        $this->assertEquals(0, static::$queue->getCount());
    }

    public function testAnItemIsRemovedFromTheFrontOfTheQueue()
    {
        static::$queue->push('Kostas');
        static::$queue->push('Gkanatsios');

        $this->assertEquals('Kostas', static::$queue->pop());
        $this->assertEquals('Gkanatsios', static::$queue->pop());
    }
}

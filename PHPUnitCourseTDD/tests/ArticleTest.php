<?php

use PHPUnit\Framework\TestCase;
use App\Article;

class ArticleTest extends TestCase
{
    protected $article;

    protected function setUp(): void
    {
        $this->article = new Article;
    }

    protected function tearDown(): void
    {
        unset($this->article);
    }

    public function testTitleIsEmptyByDefault()
    {
        $this->assertEmpty($this->article->title);
    }


    public function testSlugIsEmptyWithNoTitle()
    {
        $this->assertSame('', $this->article->getSlug());
    }

    /*
    public function testSlugHasSpacesReplacedByUnderscores()
    {
        $this->article->title = "My new article";
        $this->assertEquals('My_new_article', $this->article->getSlug());
    }

    public function testSlugHasWhitespaceReplacedBySingleUnderscore()
    {
        $this->article->title = "My    new   \n    article";
        $this->assertEquals('My_new_article', $this->article->getSlug());
    }

    public function testSlugDoesNotStartOrEndWithAnUnderscore()
    {
        $this->article->title = "   My    new   \n    article   \n ";
        $this->assertEquals('My_new_article', $this->article->getSlug());
    }

    public function testSlugDoesNotHaveAnyNonWordCharacters()
    {
        $this->article->title = "My new (1) article";
        $this->assertEquals('My_new_1_article', $this->article->getSlug());
    }
    */

    public function titleProvider()
    {
        return [
            "Slug Has Spaces Replaced By Underscores" => ["My new article", "My_new_article"],
            "Slug Has Whitespace Replaced By Single Underscore" => ["My    new   \n    article", "My_new_article"],
            "Slug Does Not Start Or End With An Underscore" => ["   My    new   \n    article   \n", "My_new_article"],
            "Slug Does Not Have Any Non Word Characters" => ["My new (1) article", "My_new_1_article"]
        ];
    }

    /**
     * @dataProvider titleProvider
     */

    public function testSlug($title, $slug)
    {
        $this->article->title = $title;
        $this->assertEquals($slug, $this->article->getSlug());
    }
}

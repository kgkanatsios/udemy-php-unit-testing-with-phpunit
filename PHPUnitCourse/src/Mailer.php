<?php
class Mailer
{
    public function sendMessage($email, $message)
    {
        if (empty($email)) {
            throw new Exception('Email address is null!');
        }
        sleep(3);
        echo "Send ${message} to ${email}";
        return true;
    }
    public static function send(string $email, string $message)
    {
        if (empty($email)) {
            throw new InvalidArgumentException('Email address is null!');
        }
        echo "Send ${message} to ${email}";
        return true;
    }
}
